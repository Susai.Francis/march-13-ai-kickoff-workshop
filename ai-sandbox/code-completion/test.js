// partial implementation of FizzBuzz
function fizzBuzz(n) {
    const answer = []

    for (let i = 1; i <= n; i++) {
        if (i % 3 == 0 && i % 5 == 0)
            answer.push("FizzBuzz")

        return answer
    }
}
